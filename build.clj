;; SPDX-License-Identifier: Apache-2.0
;; SPDX-FileCopyrightText: 2022 Sebastian Crane <seabass-labrax@gmx.com>

(ns build
  (:require [clojure.tools.build.api :as b]
            [tools-pom.tasks :as pom]))

(def application 'org.libregaming/matchbot)
(def version "1.1.1-SNAPSHOT")
(def src-dirs ["src"])
(def target-dir "target")
(def class-dir (format "%s/%s" target-dir "classes"))
(def basis (b/create-basis {:project "deps.edn"}))
(def pom-file (format "%s/pom.xml" target-dir))
(def jar-file (format "%s/%s-%s.jar" target-dir (name application) version))
(def uber-file (format "%s/%s-%s-standalone.jar" target-dir (name application) version))

(defn clean [_]
  (b/delete {:path target-dir}))

(defn uber [_]
  (b/delete {:path class-dir})
  (b/copy-dir {:src-dirs src-dirs
               :target-dir class-dir})
  (b/compile-clj {:basis basis
                  :src-dirs src-dirs
                  :class-dir class-dir})
  (b/uber {:class-dir class-dir
           :uber-file uber-file
           :basis basis
           :main 'system}))

(defn jar [_]
  (b/delete {:path class-dir})
  (b/copy-dir {:src-dirs src-dirs
               :target-dir class-dir})
  (b/jar {:class-dir class-dir
          :jar-file jar-file}))

(defn pom [_]
  (pom/pom
   {:lib application
    :version version
    :write-pom true
    :validate-pom true
    :pom
    {:description
     "A chatbot for announcing upcoming matches and finding fellow players, written for the LibreGaming community"
     :url
     "https://git.libregaming.org/LibreGaming/matchbot"
     :licenses
     [:license
      {:name "Apache-2.0"
       :url "https://www.apache.org/licenses/LICENSE-2.0.html"}]
     :developers
     [:developer
      {:id "seabass"
       :name "Sebastian Crane"
       :email "seabass-labrax@gmx.com"
       :organization "LibreGaming"
       :organization-url "https://libregaming.org"
       :roles [:role "Maintainer"]
       :timezone "Europe/London"}]
     :scm
     {:url "https://git.libregaming.org/LibreGaming/matchbot"
      :connection "scm:git:https://git.libregaming.org/LibreGaming/matchbot.git"
      :developer-connection "scm:git:ssh://git@git.libregaming.org/LibreGaming/matchbot.git"}
     :issue-management
     {:system "Gitea"
      :url "https://git.libregaming.org/LibreGaming/matchbot/issues"}}})
  (b/copy-file {:src "pom.xml" :target pom-file})
  (b/delete {:path "pom.xml"}))

(defn all [_]
  (jar nil)
  (uber nil)
  (pom nil))
