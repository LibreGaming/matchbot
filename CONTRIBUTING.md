<!--- SPDX-License-Identifier: Apache-2.0 -->
<!--- SPDX-FileCopyrightText: 2022 Sebastian Crane <seabass-labrax@gmx.com> -->

# Contributing to `matchbot`

Thank you for your interest in improving `matchbot`!
Here are some guidelines that will be useful to keep in mind through the process of submitting changes.

1. Make a personal fork of this repository on the [LibreGaming Git server](https://git.libregaming.org/) to push changes to (ask for an account on the Git server if you don't already have one).

2. Write meaningful Git commit messages - [here is a good guide for that](https://cbea.ms/git-commit/).

3. After pushing your changes to your personal fork, make a pull request with the web interface or with the [`tea` command-line client](https://gitea.com/gitea/tea).
Make sure to wait a few days to let others comment on your changes.

4. If there are no objections and you've resolved any issues that might have been brought up, rebase the commits onto the `master` branch.
If there are many commits, consider 'squashing' them into one with a single meaningful message when rebasing.

## Licensing and copyright

`matchbot` is open source software, allowing anyone to use it without restriction.
In order to maintain this, please take the time to ensure that your contributions are properly licensed.

### Developer Certificate of Origin

Please include a '`Signed-off-by:`' line in the message of each commit to show that you have agreed to the [Developer Certificate of Origin, version 1.1](https://developercertificate.org/).
You can add this automatically by using the `--signoff` flag when running `git commit`.

### REUSE headers

`matchbot` follows the [REUSE specification](https://reuse.software/) to make it easy for people to find out its licensing and copyright.

As such, all files should start with a comment of the file's licence:

> `;; SPDX-License-Identifier: Apache-2.0`

When you modify a file, add a copyright header comment to it with the year when you first modified the file, your name and finally your email address.
For example:

> `;; SPDX-FileCopyrightText: 2022 Joe Bloggs <joe@example.com>`

If the copyright to your contributions is held by your employer, put your employer's name in brackets after your own name.

## Build system

### Building a POM file

The POM file lists the dependencies needed to run `matchbot` as well as some additional information that can help people learn more about `matchbot`.
To generate a POM file, run `clojure -T:build pom`; you should find the generated `POM.xml` file in the `target/` directory.

### Building a JAR file

A JAR file contains all the source code of `matchbot` in a form that the JVM can load and pass to the Clojure compiler to run.
To generate a JAR file, run `clojure -T:build jar`; again, you should find the JAR file called something like `matchbot-x.x.x.jar` in the `target/` directory.

### Building an uberjar

An uberjar is much like a normal JAR file, but comes with all the dependencies of `matchbot` bundled in it.
This means that it can run directly on the JVM without Clojure being installed (it contains a copy of the Clojure compiler itself).
To generate an uberjar, run `clojure -T:build uber`; you should find the uberjar called something like `matchbot-x.x.x-standalone.jar` in the `target/` directory.
Please note that if you distribute an uberjar, you must not only comply with the licence of `matchbot`, but also the licences of all `matchbot`'s dependencies, both transitive and intransitive.

## Versioning and release process

### Semantic Versioning

`matchbot` uses [version 2.0.0 of the Semantic Versioning](https://semver.org/spec/v2.0.0.html) scheme, but there is still ambiguity in what exactly comprises the 'Public API' (used for determining the right part of the version to increment) for something like `matchbot`.
This 'Public API' is defined for `matchbot` as everything that is accessible by the end user or administrator of a `matchbot` instance.
For example, a change that requires the configuration file to be updated warrants a major version increment because it effects the administrator; however, a change to the structure of the internal namespaces would only require a patch level version increment because it doesn't affect either the administrator or the end user.

### Release process

At some point after a new feature has been added to `matchbot` or a bug has been fixed, a release will be made.
Once a suitable version increment for the type of changes has been determined, the `CHANGELOG.md` file at the root of the repository will be updated with release notes documenting the changes made in that version.
Then, a JAR file and a POM file will be produced using the build system (see above for more information), signed using GPG and finally uploaded to [Maven Central](https://central.sonatype.org/).
Currently this process is done by [Sebastian Crane](https://git.libregaming.org/seabass); if you would like to help with making releases, please familiarise yourself with the process (you can try everything locally except upload to Maven Central) and get in contact! 😀
