<!--- SPDX-License-Identifier: Apache-2.0 -->
<!--- SPDX-FileCopyrightText: 2022 Sebastian Crane <seabass-labrax@gmx.com> -->

# matchbot

A chatbot for announcing upcoming matches and finding fellow players, written for the [LibreGaming](https://libregaming.org) community

## Running

1. Install Clojure - you can use [Clojure's official installation guide](https://clojure.org/guides/getting_started) or your distribution's package.

2. Clone this repository:

``` bash
git clone https://git.libregaming.org/LibreGaming/matchbot.git
cd matchbot
```

3. Create a configuration file, `config.yaml`, in YAML format:

``` yaml
data-file: "matchbot-data.json"
irc:
  server: example.com
  port: 6667
  nick: matchbot
  channel: "##matchbot-test"
```

4. Run `matchbot`:

``` bash
clojure -M -m system
```

## Usage

**`!list`** - show all the games that have a list of players

**`!match game`** - announce a match of `game` to all of its players

**`!players game`** - show all the players of `game`

**`!add game`** - add yourself to the list of players for `game`

**`!remove game`** - remove yourself from the list of players for `game`

**`!help`** - show available commands with their explanations

## Development

### Running the tests

`matchbot` includes automated unit tests for many functions, along with a simple test runner configuration with [Kaocha](https://cljdoc.org/d/lambdaisland/kaocha/).
Running the tests is a similar process to running the main application:

```bash
clojure -M:test
```

### Starting a development REPL

When developing `matchbot`, it can be more convenient and efficient to work interactively with a REPL integrated into your text editor.
Since `matchbot` uses Clojure's [tools.deps library](https://clojure.org/guides/deps_and_cli), the best way to start is by following [the relevant part of the nREPL documentation](https://nrepl.org/nrepl/usage/server.html#using-clojure-cli-tools) to create a user-level `deps.edn` file, then install the [nREPL extension for your text editor](https://nrepl.org/nrepl/usage/clients.html#editorsides).

You can create, start and stop an instance of the chatbot process with the functions in the `system` namespace:

``` clojure
;; creating a new instance - an empty Var
(def my-instance nil)

;; starting the instance
(alter-var-root #'my-instance system/start)

;; restarting the instance
(system/restart #'my-instance)

;; stopping and resetting the instance
(alter-var-root #'my-instance system/stop)
(alter-var-root #'my-instance (constantly nil))
```

Once you are familiar with nREPL, you can additionally use [tools.namespace.repl](https://github.com/clojure/tools.namespace) to make reevaluating (reloading) your changes easier:

``` clojure
(require '[clojure.tools.namespace.repl :refer [refresh]])
(refresh)
```

Make sure that you create your system instance in the REPL, since `refresh` will forcefully delete the instance if it has been defined in another namespace!
