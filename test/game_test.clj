;; SPDX-License-Identifier: Apache-2.0
;; SPDX-FileCopyrightText: 2021 Sebastian Crane <seabass-labrax@gmx.com>

(ns game-test
  (:require [clojure.test :refer :all]
            [game :refer :all]))

(def test-state '{:games {"hypothetical-shooter" #{"player-one" "player-two" "player-three"}
                          "quasi-rts" #{"player-two" "player-four"}
                          "imaginary-rpg" #{"player-one" "player-three" "player-four"}}})

(deftest get-players-of-game-test
  (is (=
       '#{"player-two" "player-four"}
       (get-players-of-game test-state "quasi-rts"))))

(deftest add-player-of-game-test
  (is (=
       '#{"player-one" "player-two" "player-four"}
       (get-in (add-player-of-game test-state "quasi-rts" "player-one") [:games "quasi-rts"]))))

(deftest remove-player-of-game-test
  (is (=
       '#{"player-one" "player-three"}
       (get-in (remove-player-of-game test-state "imaginary-rpg" "player-four") [:games "imaginary-rpg"]))))

(deftest get-games-test
  (is (=
       '#{"hypothetical-shooter" "quasi-rts" "imaginary-rpg"}
       (set (get-games test-state)))))

(deftest remove-game-test
  (is (=
       '#{"hypothetical-shooter" "imaginary-rpg"}
       (set (keys (:games (remove-game test-state "quasi-rts")))))))
