;; SPDX-License-Identifier: Apache-2.0
;; SPDX-FileCopyrightText: 2022 Sebastian Crane <seabass-labrax@gmx.com>

(ns bot-test
  (:require [clojure.test :refer :all]
            [bot :refer :all]))

(def test-state '{:games {"hypothetical-shooter" #{"abc" "xyz" "123"}
                          "quasi-rts" #{"abc" "123"}
                          "imaginary-rpg" #{"xyz" "abc"}}})

(deftest lower-case-game-test
  (is (= "quasi-rts"
         (lower-case-game "Quasi-RTS"))))

(deftest sort-case-insensitive-test
  (is (= ["A" "b" "C"]
         (sort-case-insensitive ["C" "A" "b"]))))

(deftest match-string-test
  (is (= '"Anyone ready for quasi-Rts? abc"
         (match-string :state test-state :game "quasi-Rts" :player "123"))))

(deftest list-players-string-test
  (is (= "Players of HYPOTHETICAL-shooter: _123_ _abc_ _xyz_"
         (list-players-string :state test-state :game "HYPOTHETICAL-shooter"))))

(deftest add-player-string-test
  (is (= "Added abc to the list of players for Quasi-Rts!"
         (add-player-string :game "Quasi-Rts" :player "abc"))))

(deftest remove-player-string-test
  (is (= "Removed abc from the list of players for hypothetical-shooter!"
         (remove-player-string :game "hypothetical-shooter" :player "abc"))))

(deftest list-games-string-test
  (is (= "Games with a list of players: hypothetical-shooter, imaginary-rpg, quasi-rts"
         (list-games-string :state test-state))))

(deftest split-message-test
  (is (= {:command "!match" :game "Quasi-Rts" :game-keyword "quasi-rts"}
         (split-message "!match Quasi-Rts  "))))

(deftest dispatch-command-test
  (let [state-atom (atom test-state)]
    (is (and
         (= "Added 123 to the list of players for Imaginary-RPG!"
            (dispatch-command state-atom "123" "!add Imaginary-RPG"))
         (= {:games {"hypothetical-shooter" #{"abc" "xyz" "123"}
                     "quasi-rts" #{"abc" "123"}
                     "imaginary-rpg" #{"xyz" "abc" "123"}}}
            @state-atom)))))
