<!--- SPDX-License-Identifier: Apache-2.0 -->
<!--- SPDX-FileCopyrightText: 2022 Sebastian Crane <seabass-labrax@gmx.com> -->

# Changelog for `matchbot`

`matchbot` uses [version 2.0.0 of the Semantic Versioning](https://semver.org/spec/v2.0.0.html) scheme.

## [1.1.0] - 2022-05-14

* Add build system for generating POM, JAR and uberjar (standalone JAR) files

* Improve code quality

## [1.0.1] - 2022-04-01

* Fix vulnerability that causes serialised data to be deleted when it contains certain user input

* Change internal representation of game names, fixing a potential memory leak for long-running instances

* Improve reliability and code quality of the startup and shutdown sequences

## [1.0.0] - 2022-03-03

* Initial release

[1.1.0]: https://git.libregaming.org/LibreGaming/matchbot/releases/tag/1.1.0
[1.0.1]: https://git.libregaming.org/LibreGaming/matchbot/releases/tag/1.0.1
[1.0.0]: https://git.libregaming.org/LibreGaming/matchbot/releases/tag/1.0.0
