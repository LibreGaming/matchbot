;; SPDX-License-Identifier: Apache-2.0
;; SPDX-FileCopyrightText: 2022 Sebastian Crane <seabass-labrax@gmx.com>

(ns irc
  (:require [clojure.string :as str]
            [bot :refer :all]
            [irclj.core]))

(defn irc-callback [state config connection type & m]
  (let [{:keys [channel]} (:irc config)
        {:keys [nick text target]} type]
    (some->> (dispatch-command state nick text)
             (str/split-lines)
             (run! (partial irclj.core/message connection channel)))))

(defn new-irc-connection [state config]
  (let [{:keys [server port name nick channel]} (:irc config)]
    (try
      (irclj.core/connect server
       port
       nick
       :real-name (or name nick)
       :callbacks {:privmsg (partial irc-callback state config)})
      (catch Exception e nil))))
