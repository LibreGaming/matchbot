;; SPDX-License-Identifier: Apache-2.0
;; SPDX-FileCopyrightText: 2021 Sebastian Crane <seabass-labrax@gmx.com>

(ns game
  (:require [clojure.set :refer [superset?]]))

(defn get-players-of-game [state game]
  (get-in state [:games game]))

(defn add-player-of-game [state game player]
  (update-in state [:games game] #(set (conj % player))))

(defn remove-player-of-game [state game player]
  (update state :games
          #(if (superset? (set [player]) (% game))
             (dissoc % game)
             (update % game disj player))))

(defn get-games [state]
  (keys (:games state)))

(defn remove-game [state game]
  (update-in state [:games] dissoc game))
