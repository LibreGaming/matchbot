;; SPDX-License-Identifier: Apache-2.0
;; SPDX-FileCopyrightText: 2022 Sebastian Crane <seabass-labrax@gmx.com>

(ns system
  (:require [irc]
            [clojure.data.json :as json]
            [clojure.set :as set]
            [clj-yaml.core :as yaml])
  (:gen-class))

(defn setify-vals [x]
  (reduce #(assoc %1
                  (first %2)
                  (set (second %2)))
          {} x))

(defn process-json [x]
  (-> (set/rename-keys x {"games" :games})
      (update :games setify-vals)))

(defn load-state [f]
  (process-json
   (try
     (with-open [datafile (clojure.java.io/reader f)]
       (json/read datafile))
     (catch Exception e nil))))

(defn save-state [f data]
  (try
    (with-open [datafile (clojure.java.io/writer f)]
      (json/write data datafile))
    (catch Exception e nil)))

(defn load-config [f]
  (try
    (with-open [datafile (clojure.java.io/reader f)]
      (yaml/parse-stream datafile))
    (catch Exception e nil)))

(defn start [_]
  (let [config (load-config "config.yaml")
        state (atom (load-state (:data-file config)))
        irc (irc/new-irc-connection state config)]
    (irclj.core/join irc (get-in config [:irc :channel]))
    {:config config
     :state state
     :irc irc}))

(defn stop [system]
  (save-state
   (get-in system [:config :data-file])
   (deref (:state system)))
  (irclj.core/quit (system :irc)))

(defn restart [system-var]
  (stop (deref system-var))
  (alter-var-root system-var start))

(defn -main [& args]
  (let [main-system (system/start nil)]
    (.addShutdownHook (Runtime/getRuntime)
                      (Thread. #(stop main-system)))))
