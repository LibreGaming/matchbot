;; SPDX-License-Identifier: Apache-2.0
;; SPDX-FileCopyrightText: 2022 Sebastian Crane <seabass-labrax@gmx.com>

(ns bot
  (:require [clojure.string :as str]
            [game]
            [irclj.core]))

(defn lower-case-game [game]
  (when (string? game)
    (str/lower-case game)))

(defn sort-case-insensitive [coll]
  (sort #(apply compare (map str/lower-case %&)) coll))

(defn match-string [& {:keys [state game player]}]
  (as-> (lower-case-game game) x
    (game/get-players-of-game state x)
    (disj x player)
    (sort-case-insensitive x)
    (str/join " " x)
    (str "Anyone ready for " game "? " x)))

(defn list-players-string [& {:keys [state game]}]
  (as-> (lower-case-game game) x
    (game/get-players-of-game state x)
    (sort-case-insensitive x)
    (map #(str " _" % "_") x)
    (apply str "Players of " game ":" x)))

(defn add-player-string [& {:keys [game player]}]
  (str "Added "
       player
       " to the list of players for "
       game
       "!"))

(defn remove-player-string [& {:keys [game player]}]
  (str "Removed "
       player
       " from the list of players for "
       game
       "!"))

(defn list-games-string [& {:keys [state]}]
  (str "Games with a list of players: "
       (str/join
        ", "
        (sort-case-insensitive (game/get-games state)))))

(defn help-string [& {:keys []}]
  " !list - show all the games that have a list of players
 !match game - announce a match of game to all of its players
 !players game - show all the players of game
 !add game - add yourself to the list of players for game
 !remove game - remove yourself from the list of players for game")

(defn split-message [message]
  (let [message-parts (str/split message #"\s")
        command (if-let [x (first message-parts)] (str/lower-case x) "")
        game (second message-parts)
        game-keyword (lower-case-game game)]
    {:command command
     :game game
     :game-keyword game-keyword}))

(defn dispatch-command [state sender message]
  (let [{command :command
         game :game
         game-keyword :game-keyword} (split-message message)]
    (if game
      (condp = command
        "!match"
        (match-string :state @state :game game :player sender)

        "!add"
        (do (swap! state game/add-player-of-game game-keyword sender)
            (add-player-string :game game :player sender))

        "!players"
        (list-players-string :state @state :game game)

        "!remove"
        (do (swap! state game/remove-player-of-game game-keyword sender)
            (remove-player-string :game game :player sender))
        nil)

      (condp = command
        "!list"
        (list-games-string :state @state)

        "!help"
        (help-string)
        nil))))
